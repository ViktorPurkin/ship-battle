import {BGE} from '../game/bge';
import {Inject, Injectable} from '@angular/core';
import {BGE_INJECTION_TOKEN} from '../game/bge-injection-token';
import {IGbe} from '../game/igbe';

@Injectable()
export class GameService {

  constructor(@Inject(BGE_INJECTION_TOKEN) public bge: IGbe) {
  }

  createNewGame(): void {
    this.bge.startGame();
  }
}
