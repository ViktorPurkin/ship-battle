import {Pipe, PipeTransform} from '@angular/core';
import {CellType} from '../game/cell-type';

@Pipe({
  name: 'cssStyle'
})
export class CssStylePipe implements PipeTransform {

  transform(cellType: CellType): string {
    return CellType[cellType];
  }

}
