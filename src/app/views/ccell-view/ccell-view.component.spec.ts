import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CCellViewComponent } from './ccell-view.component';

describe('CCellViewComponent', () => {
  let component: CCellViewComponent;
  let fixture: ComponentFixture<CCellViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CCellViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CCellViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
