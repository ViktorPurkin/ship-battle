import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {CCell} from '../../game/ccell';

@Component({
  selector: 'app-ccell-view',
  templateUrl: './ccell-view.component.html',
  styleUrls: ['./ccell-view.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CCellViewComponent {
  @Input() cell: CCell;
}
