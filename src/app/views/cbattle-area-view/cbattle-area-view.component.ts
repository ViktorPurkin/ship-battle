import {Component, OnInit, Output, Input, EventEmitter, ChangeDetectionStrategy} from '@angular/core';
import {CBattleArea} from '../../game/cbattle-area';
import {CCell} from '../../game/ccell';

@Component({
  selector: 'app-cbattle-area-view',
  templateUrl: './cbattle-area-view.component.html',
  styleUrls: ['./cbattle-area-view.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CBattleAreaViewComponent {

  @Input() battleArea: CBattleArea;
  @Input() isActive = true;
  @Output() cellClick: EventEmitter<CCell> = new EventEmitter<CCell>();


  clickOnCell(cell: CCell): void {
    if (this.isActive) {
      this.cellClick.emit(cell);
    }
  }

  trackByCellValue(index, cell: CCell): string {
    return index + '_' + cell.cellType;
  }
}
