import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CBattleAreaViewComponent } from './cbattle-area-view.component';

describe('CBattleAreaViewComponent', () => {
  let component: CBattleAreaViewComponent;
  let fixture: ComponentFixture<CBattleAreaViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CBattleAreaViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CBattleAreaViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
