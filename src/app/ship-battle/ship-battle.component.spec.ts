import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipBattleComponent } from './ship-battle.component';

describe('ShipBattleComponent', () => {
  let component: ShipBattleComponent;
  let fixture: ComponentFixture<ShipBattleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShipBattleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipBattleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
