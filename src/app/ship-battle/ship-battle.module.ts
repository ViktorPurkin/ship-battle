import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ShipBattleRoutingModule} from './ship-battle-routing.module';
import {ShipBattleComponent} from './ship-battle.component';
import {GameService} from '../services/game.service';
import {CBattleAreaViewComponent} from '../views/cbattle-area-view/cbattle-area-view.component';
import {CCellViewComponent} from '../views/ccell-view/ccell-view.component';
import {CssStylePipe} from '../pipes/css-style.pipe';
import {COUNTS_SHIPS, SIZE_OF_SHIPS} from '../game/settings-injection-token';
import {BGE_INJECTION_TOKEN} from '../game/bge-injection-token';
import {BGE} from '../game/bge';
import {IGbe} from '../game/igbe';


@NgModule({
  declarations: [
    ShipBattleComponent,
    CBattleAreaViewComponent,
    CCellViewComponent,
    CssStylePipe
  ],
  imports: [
    CommonModule,
    ShipBattleRoutingModule
  ],
  providers: [
    {provide: COUNTS_SHIPS, useValue: 4},
    {provide: SIZE_OF_SHIPS, useValue: 3},
    // TO READ. а можно было и одним обьектом типа
    // {
    //   provide: GAME_SETTINGS, useValue: {
    //     COUNTS_SHIPS: 3, SIZE_OF_SHIPS: 3,
    //     player1: new CPlayer('Vasa'), player2: new CPlayer('Vasa2')
    //   }
    // },
    // не доделал пункт 1 и 17 связанными с DI потому что
    // я не знаю на правильном ли я пути
    //
    {
      provide: BGE_INJECTION_TOKEN,
      useFactory: (countShips: number, sizeOfShips: number): IGbe => new BGE(countShips, sizeOfShips),
      deps: [COUNTS_SHIPS, SIZE_OF_SHIPS]
    },
    GameService,

  ]
})
export class ShipBattleModule {
}
