import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {GameService} from '../services/game.service';
import {CCell} from '../game/ccell';
import {CPlayer} from '../game/cplayer';
import {CBattleArea} from '../game/cbattle-area';

@Component({
  selector: 'app-ship-battle',
  templateUrl: './ship-battle.component.html',
  styleUrls: ['./ship-battle.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShipBattleComponent {
  isShowShip = false;
  winner: CPlayer = null;

  areaFirstToShow: CBattleArea = null;
  areaSecondToShow: CBattleArea = null;
  areaFirst: CBattleArea = null;
  areaSecond: CBattleArea = null;
  isActiveFirstPlayer = true;

  playerNameFirst = '';
  playerNameSecond = '';

  constructor(public gameService: GameService) {
    this.playerNameFirst = gameService.bge.player1.name;
    this.playerNameSecond = gameService.bge.player2.name;
  }


  createNewGame(): void {
    this.isShowShip = false;
    this.gameService.createNewGame();

    this.updateBattleAreas();

    this.isActiveFirstPlayer = this.gameService.bge.isActiveFirstPlayer;

  }

  toogleShowShip(): void {
    this.isShowShip = !this.isShowShip;
  }

  clickOnCell(cell: CCell): void {
    if (this.gameService.bge.getWinner() === null) {
      const {x, y} = cell;
      this.gameService.bge.shoot(x, y);
      this.winner = this.gameService.bge.getWinner();
      this.isActiveFirstPlayer = this.gameService.bge.isActiveFirstPlayer;
      this.updateBattleAreas();

    } else {
      this.showShips();
    }

  }

  showShips(): void {
    this.isShowShip = true;
  }

  private updateBattleAreas(): void {
    this.areaFirstToShow = this.gameService.bge.getArea1ToShow();
    this.areaSecondToShow = this.gameService.bge.getArea2ToShow();
    this.areaFirst = this.gameService.bge.getAreaFirst();
    this.areaSecond = this.gameService.bge.getAreaSecond();
  }
}
