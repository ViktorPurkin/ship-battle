import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ShipBattleComponent} from './ship-battle.component';


const routes: Routes = [
  { path: '', component: ShipBattleComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShipBattleRoutingModule { }
