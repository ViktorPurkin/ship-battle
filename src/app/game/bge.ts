import {CPlayer} from './cplayer';
import {CBattleArea} from './cbattle-area';
import {CellType} from './cell-type';
import {CCell} from './ccell';
import {IGbe} from './igbe';

export class BGE implements IGbe {
  player1: CPlayer = new CPlayer('Alisa');
  player2: CPlayer = new CPlayer('Bob');
  isActiveFirstPlayer = true;
  private battleArea1: CBattleArea = null;
  private battleArea2: CBattleArea = null;
  private requiredCountOfHits = this.countShips * this.sizeOfShips;
  private winner: CPlayer = null;


  constructor(public countShips: number, public sizeOfShips: number) {

  }

  startGame() {

    this.winner = null;
    this.battleArea1 = this.initBattleArea();
    this.battleArea2 = this.initBattleArea();
    this.isActiveFirstPlayer = true;
    this.player1.resetCountOfHits();
    this.player2.resetCountOfHits();
  }

  getArea1ToShow(): CBattleArea {
    return this.getAreaToShow(this.battleArea1);
  }

  getArea2ToShow(): CBattleArea {
    return this.getAreaToShow(this.battleArea2);
  }

  shoot(x: number, y: number): boolean {

    const player: CPlayer = this.shootPlayer(x, y);

    if (this.isPlayerWinner(player)) {
      this.setWinner(player);
    } else {
      this.isActiveFirstPlayer = !this.isActiveFirstPlayer;
    }

    return player.isHit;
  }

  getWinner(): CPlayer {
    return this.winner;
  }

  getAreaToShow(battleArea: CBattleArea,
                filter: (cell: CCell) => boolean = ({cellType}: CCell) => cellType === CellType.ship): CBattleArea {

    if (battleArea) {
      const ba: CBattleArea = new CBattleArea();
      ba.area = battleArea.getAreaToShow(filter);
      return ba;
    }
    return null;
  }

  getAreaFirst(): CBattleArea {
    return this.getAreaToShow(this.battleArea1, () => false);
  }

  getAreaSecond(): CBattleArea {
    return this.getAreaToShow(this.battleArea2, () => false);
  }

  private shootPlayer(x: number, y: number): CPlayer {
    let player: CPlayer;
    if (this.isActiveFirstPlayer) {
      this.player1.isHit = this.isHitPlayer(this.battleArea2, x, y);
      player = this.player1;
    } else {
      this.player2.isHit = this.isHitPlayer(this.battleArea1, x, y);
      player = this.player2;
    }

    if (player.isHit) {
      player.incrementHit();
    }

    return player;
  }

  private isHitPlayer(battleArea: CBattleArea, x: number, y: number): boolean {
    const cell: CCell = battleArea.getCell(x, y);
    let isHit = false;
    switch (cell.cellType) {
      case CellType.ship:
        cell.cellType = CellType.hit;
        isHit = true;
        break;
      case CellType.empty:
        cell.cellType = CellType.miss;
        break;
      case CellType.miss:
      case CellType.hit:
        break;
    }
    return isHit;
  }

  private initBattleArea(): CBattleArea {
    const battleArea: CBattleArea = new CBattleArea();
    battleArea.createShips(this.countShips, this.sizeOfShips);
    return battleArea;

  }

  private isPlayerWinner(player: CPlayer): boolean {
    return this.requiredCountOfHits === player.countOfHits;
  }

  private setWinner(player: CPlayer): void {
    this.winner = player;
  }


}
