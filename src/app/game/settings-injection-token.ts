import {InjectionToken} from '@angular/core';

export const COUNTS_SHIPS = new InjectionToken<number>('COUNTS_SHIPS');
export const SIZE_OF_SHIPS = new InjectionToken<number>('SIZE_OF_SHIPS');
