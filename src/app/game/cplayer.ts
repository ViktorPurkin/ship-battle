export class CPlayer {
  countOfHits: number;
  isHit: boolean;

  constructor(public name: string) {
  }

  resetCountOfHits(): void {
    this.countOfHits = 0;
  }

  incrementHit(): void {
    this.countOfHits++;
  }
}
