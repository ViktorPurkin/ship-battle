import {CellType} from './cell-type';

export class CCell {
  cellType: CellType = CellType.empty;

  constructor(public x: number, public y: number) {
  }

  isEmpty(): boolean {
    return this.cellType === CellType.empty;
  }
}
