import {CCell} from './ccell';
import {CellType} from './cell-type';
import {Direction} from './direction.enum';
import {CUtil} from './cutil';

export class CBattleArea {
  area: CCell[][] = null;
  size = 8;

  private  tplArray = Array.from({length: this.size});
  constructor() {
    this.initArea();
  }

  getCell(x: number, y: number): CCell {
    if (x < 0 || y < 0) {
      return null;
    }
    return this.area?.[x]?.[y] ?? null;
  }

  createShips(countShips: number, sizeOfShips: number): void {


    for (let i = 0; i < countShips;) {
      const startX: number = CUtil.randomInt(0, this.size);
      const startY: number = CUtil.randomInt(0, this.size);
      const direction: number = CUtil.randomInt(Direction.DOWN, Direction.RIGHT + 1);

      // console.log(direction);

      const wasCreated: boolean = this.addShip(startX, startY, sizeOfShips, direction);
      if (wasCreated) {
        i++;
      }
    }
  }

  getAreaToShow(filter: (cell: CCell) => boolean = (c: CCell) => false): CCell[][] {
    return this.area.map((row: CCell[], indexR) => {
      return row.map((cell: CCell, indexC) => filter(cell) ? new CCell(indexR, indexC) : cell);
    });
  }

  private initArea(): void {
    this.area = this.tplArray.map((value: CCell[], indexR) => {
      return  this.tplArray.map((cell: CCell, indexC) => {
        return new CCell(indexR, indexC);
      });
    });
  }
  private createShipDecks(getCellBasedOnDirection: ( index) => CCell, sizeOfShips: number, startPosition: number): boolean {
    if (startPosition + sizeOfShips >= this.size) {
      return false;
    }
    for (let i = 0; i < sizeOfShips; i++) {
      const cell: CCell = getCellBasedOnDirection(i);
      if (!cell.isEmpty() || !this.isEmptyAround(cell)) {
        return false;
      }
    }

    for (let i = 0; i < sizeOfShips; i++) {
      getCellBasedOnDirection(i).cellType = CellType.ship;
    }
    return true;
  }

  private addShip(startX: number, startY: number, sizeOfShips: number, direction: Direction): boolean {
    if (direction === Direction.RIGHT) {
      const getCellBasedOnDirection = (index) => this.getCell(startX, index + startY);
      return this.createShipDecks(getCellBasedOnDirection, sizeOfShips, startY);
    }

    if (direction === Direction.DOWN) {
      const getCellBasedOnDirection = (index) => this.getCell(startX + index,  startY);
      return this.createShipDecks(getCellBasedOnDirection, sizeOfShips, startX);
    }

  }

  private isEmptyAround(cell: CCell): boolean {
    const {x, y} = cell;
    const arr: CCell[] = [];
    arr.push(this.getCell(x + 1, y));
    arr.push(this.getCell(x - 1, y));
    arr.push(this.getCell(x, y + 1));
    arr.push(this.getCell(x, y - 1));
    arr.push(this.getCell(x + 1, y + 1));
    arr.push(this.getCell(x - 1, y - 1));
    arr.push(this.getCell(x - 1, y + 1));
    arr.push(this.getCell(x + 1, y - 1));

    return arr.every((cellItem: CCell) => {
      return cellItem?.isEmpty() ?? true;
    });

  }



}
