import {InjectionToken} from '@angular/core';
import {IGbe} from './igbe';

export const BGE_INJECTION_TOKEN = new InjectionToken<IGbe>('bgeInjectionToken');

