import {CCell} from './ccell';
import {CPlayer} from './cplayer';
import {CBattleArea} from './cbattle-area';

export interface IGbe {
  player1: CPlayer;
  player2: CPlayer;
  isActiveFirstPlayer: boolean;

  startGame(): void;

  getArea1ToShow(): CBattleArea;

  getArea2ToShow(): CBattleArea;

  shoot(x: number, y: number): boolean;

  getWinner(): CPlayer;

  getAreaToShow(battleArea: CBattleArea,
                filter: (cell: CCell) => boolean): CBattleArea;

  getAreaFirst(): CBattleArea;

  getAreaSecond(): CBattleArea;
}
