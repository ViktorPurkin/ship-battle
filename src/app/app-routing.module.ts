import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';



const routes: Routes = [
  {
    path: 'battle',
    loadChildren: () => import('./ship-battle/ship-battle.module').then(m => m.ShipBattleModule),

  },
  {path: '', redirectTo: '/battle', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
